// Express
const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const bodyParser = require('body-parser');

// SQLite
const sqlite3 = require('sqlite3').verbose();

// open database
let db = new sqlite3.Database('./db/grocery.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to the Grocery SQlite database.');
});

// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use(express.static(__dirname + '/')); // pouvoir link le main.css

// *** DATA PRODUCTS *** //
let formData = {
    id: null,
    name: '',
    thumbnail_url: '',
    desc: '',
    price: 0,
    quantity: 0
};
// *** DATA FRESH PRODUCTS *** //
let formDataFresh = {
    id: null,
    name: '',
    thumbnail_url: '',
    info: '',
    price: 0,
    quantity: 0,
    dlc: 0
};

// Initialize the products database first, only needed at first launch
// initDB();

// Initialize the fresh_products database first, only needed at first launch
// initDBFP();


// *** ROUTES *** //
/**
 * index page
 */
app.get('/', (req, res) => {
    res.render('pages/index');
});

/**
 * products page
 */
app.get('/products', (req, res) => {
    const query = `SELECT * FROM products`;
    db.all(query, [], (err, rows) => {
        if (err) {
            console.error(err.message);
        }

        res.render('pages/products', {
            products: rows
        });
    });
});

// fresh products page
app.get('/fresh_products', (req, res) => {
    const query = `SELECT * FROM fresh_products`;
    db.all(query, [], (err, rows) => {
        if (err) {
            console.error(err.message);
        }

        res.render('pages/fresh-products', {
            fresh_products: rows
        });
    });
});

/**
 * inventory page
 */
app.get('/inventory', (req, res) => {
    renderInventory(res);
    renderInventoryFresh(res);
});

app.get('/inventory/newProduct', (req, res) => {
    resetFormData();
    renderInventory(res, true);
});

app.get('/inventory/newFreshProduct', (req, res) => {
    resetFormDataFresh();
    renderInventoryFresh(res, true);
});

/**
 * about page
 */
app.get('/about', (req, res) => {
    res.render('pages/about');
});

// *** CRUD *** //
/**
 * create product
 */
app.post('/inventory/create', (req, res) => {
    const query = `INSERT INTO products (name, thumbnail_url, info, price, quantity)
        VALUES ('${req.body.inputName}', 
            '${req.body.inputThumbNail}',
            '${req.body.inputDesc}',
            ${req.body.inputPrice},
            ${req.body.inputQuantity}
        )`;

    db.run(query, (err) => {
        if (err) {
            return console.error('ici: ', err.message);
        }
        console.log('Fresh product successfully created.');

        renderInventory(res);
    });
});

// create fresh product
app.post('/inventory/createFresh', (req, res) => {
    const query = `INSERT INTO fresh_products (name, thumbnail_url, info, price, quantity, dlc)
        VALUES ('${req.body.inputFreshName}', 
            '${req.body.inputFreshThumbNail}',
            '${req.body.inputFreshInfo}',
            ${req.body.inputFreshPrice},
            ${req.body.inputFreshQuantity},
            ${req.body.inputFreshDlc}
        )`;

    db.run(query, (err) => {
        if (err) {
            return console.error('ici: ', err.message);
        }
        console.log('Fresh product successfully created.');

        renderInventoryFresh(res);
    });
});

/**
 * read product
 */
app.get('/inventory/:id', (req, res) => {
    if (isNaN(req.params.id)) {
        return;
    }

    const query = `SELECT * FROM products WHERE id = ${req.params.id}`;
    db.all(query, [], (err, rows) => {
        if (err) {
            console.error(err.message);
        }

        if (!rows) {
            renderInventory(res);
            return;
        }

        const data = rows[0];

        formData = {
            id: data.id,
            name: data.name,
            thumbnail_url: data.thumbnail_url,
            desc: data.info,
            price: data.price,
            quantity: data.quantity
        }

        res.render('pages/inventory', {
            products: rows,
            formData: formData,
            showCreateProduct: false,
            showUpdateProduct: true
        });
    });
});

/**
 * read fresh product
 */
app.get('/inventory/fresh-:id', (req, res) => {
    if (isNaN(req.params.id)) {
        return;
    }

    const query = `SELECT * FROM fresh_products WHERE id = ${req.params.id}`;
    db.all(query, [], (err, rows) => {
        if (err) {
            console.error(err.message);
        }

        if (!rows) {
            renderInventoryFresh(res);
            return;
        }

        const data = rows[0];

        formDataFresh = {
            id: data.id,
            name: data.name,
            thumbnail_url: data.thumbnail_url,
            info: data.info,
            price: data.price,
            quantity: data.quantity,
            dlc: data.dlc
        }

        res.render('pages/inventory', {
            products: rows,
            formDataFresh: formDataFresh,
            showCreateFreshProduct: false,
            showUpdateFreshProduct: true
        });
    });
});

/**
 * update product
 */
app.post('/inventory/update/:id', (req, res) => {
    const query = `UPDATE products
        SET name = '${req.body.inputName}',
        thumbnail_url = '${req.body.inputThumbNail}',
        info = '${req.body.inputDesc}',
        price = ${req.body.inputPrice},
        quantity = ${req.body.inputQuantity}
        WHERE id = ${req.params.id}
        `;

    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Products successfully updated.');

        resetFormData();

        renderInventory(res);
    });
});

/**
 * update fresh product
 */
app.post('/inventory/update/fresh/:id', (req, res) => {
    const query = `UPDATE fresh_products
        SET name = '${req.body.inputFreshName}',
        thumbnail_url = '${req.body.inputFreshThumbNail}',
        info = '${req.body.inputFreshInfo}',
        price = ${req.body.inputFreshPrice},
        quantity = ${req.body.inputFreshQuantity},
        dlc = ${req.body.inputFreshDlc},
        WHERE id = ${req.params.id}
        `;

    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Fresh products successfully updated.');

        resetFormDataFresh();

        renderInventoryFresh(res);
    });
});

/**
 * delete product
 */
app.get('/inventory/delete/:id', (req, res) => {
    if (isNaN(req.params.id)) {
        return;
    }

    const query = `DELETE FROM products WHERE id = ${req.params.id};`;

    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Products successfully deleted.');

        renderInventory(res);
    });
});
/**
 * delete fresh product
 */
app.get('/inventory/delete/fresh/:id', (req, res) => {
    if (isNaN(req.params.id)) {
        return;
    }

    const query = `DELETE FROM fresh_products WHERE id = ${req.params.id};`;

    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Fresh product successfully deleted.');

        renderInventoryFresh(res);
    });
});

/**
 * Listen for connections
 */
app.listen(port, () => {
    console.log(`Epicerie app listening at http://localhost:${port}`)
});

// *** QUERIES *** //

/**
 * Create the table products
 */
function initDB() {
    const query = `CREATE TABLE IF NOT EXISTS products (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
        thumbnail_url TEXT NOT NULL,
        info TEXT NOT NULL,
        price INTEGER NOT NULL,
        quantity INTEGER NOT NULL
    )`;

    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Table products successfully created.');
        populateDB();
    });
}

// CREATE FRESH_PRODUCTS TABLE
function initDBFP() {
    const query = `CREATE TABLE IF NOT EXISTS fresh_products (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
        thumbnail_url TEXT NOT NULL,
        info TEXT NOT NULL,
        price INTEGER NOT NULL,
        quantity INTEGER NOT NULL,
        dlc DATE NOT NULL
    )`;

    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Table fresh_products successfully created.');
        populateDBFP();
    });
}


/**
 * Add default products to the table products
 */
function populateDB() {
    const query = `INSERT INTO products (name, thumbnail_url, info, price, quantity)
        VALUES ('Pepito', 
        'https://cdn.mcommerce.franprix.fr/product-images/3017760314497_A1L1_s03',
        'Pépito La Boîte A Goûter 540g',
        5, 10)`;

    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Table products successfully populated.');
    });
}
/**
 * Add default products to the table fresh_products
 */
function populateDBFP() {
    const query = `INSERT INTO fresh_products (name, thumbnail_url, info, price, quantity, dlc)
        VALUES ('Raviolis', 
        'https://cdn.mcommerce.franprix.fr/product-images/3017760314497_A1L1_s03',
        'Raviolis frais 250g basilic et ricotta',
        4.98, 12, '27/01/2021')`;

    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Table products successfully populated.');
    });
}


// *** UTILITIES *** //
function renderInventory(res, showCreateProduct = false) {
    resetFormData();

    if (showCreateProduct) {
        res.render('pages/inventory', {
            products: [],
            formData: formData,
            showCreateProduct: showCreateProduct,
            showUpdateProduct: false
        });

        return;
    }

    const query = `SELECT * FROM products`;
    db.all(query, [], (err, rows) => {
        if (err) {
            console.error('here: ', err.message);
        }

        res.render('pages/inventory', {
            products: rows,
            formData: formData,
            showCreateProduct: showCreateProduct,
            showUpdateProduct: false
        });
    });
}

function renderInventoryFresh(res, showCreateFreshProduct = false) {
    resetFormDataFresh();

    if (showCreateFreshProduct) {
        res.render('pages/inventory', {
            fresh_products: [],
            formDataFresh: formDataFresh,
            showCreateFreshProduct: showCreateFreshProduct,
            showUpdateFreshProduct: false
        });

        return;
    }

    const query = `SELECT * FROM fresh_products`;
    db.all(query, [], (err, rows) => {
        if (err) {
            console.error('here: ', err.message);
        }

        res.render('pages/inventory', {
            fresh_products: rows,
            formDataFresh: formDataFresh,
            showCreateFreshProduct: showCreateFreshProduct,
            showUpdateFreshProduct: false
        });
    });
}

function resetFormData() {
    formData = {
        id: null,
        name: '',
        thumbnail_url: '',
        desc: '',
        price: 0,
        quantity: 0
    };
}

function resetFormDataFresh() {
    formDataFresh = {
        id: null,
        name: '',
        thumbnail_url: '',
        info: '',
        price: 0,
        quantity: 0,
        dlc: 0
    };
}